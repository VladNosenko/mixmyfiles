const basePath = process.cwd();
const buildDir = `${basePath}/build`;
const imgDir = `${basePath}/assets/images`;;
const jsonDir = `${basePath}/assets/json`;

const collectionSize = 50;

module.exports = {
    buildDir,
    imgDir,
    jsonDir,
    collectionSize,
};