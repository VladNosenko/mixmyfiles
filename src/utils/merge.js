const basePath = process.cwd();
const fs = require("fs");

const { buildDir  } = require(`${basePath}/src/config.js`);
const mergeDir = `${buildDir}/assets`;

const mergeBuildedAssets = () => {
    if(!fs.existsSync(buildDir)) {
        return console.log("\x1b[31m", new Error("Build directory is missing. Run generate command first!"));
    }
    if (fs.existsSync(mergeDir)) {
       fs.rmdirSync(mergeDir, { recursive: true });
    }

    fs.mkdirSync(mergeDir);
    
    if(process.env.CLEAN) {
        fs.readdirSync(`${buildDir}/json`).forEach((_, index) => {
            fs.rename(`${buildDir}/json/${index}.json`, `${mergeDir}/${index}.json`, (err) => {err && console.log(err);} )
            fs.rename(`${buildDir}/images/${index}.png`, `${mergeDir}/${index}.png`, (err) => {err && console.log(err);} )
        })
        fs.rmdirSync(`${buildDir}/json`, { recursive: true });
        fs.rmdirSync(`${buildDir}/images`, { recursive: true });
    } else {
        fs.readdirSync(`${buildDir}/json`).forEach((_, index) => {
            fs.copyFile(`${buildDir}/json/${index}.json`, `${mergeDir}/${index}.json`, (err) => {err && console.log(err);} )
            fs.copyFile(`${buildDir}/images/${index}.png`, `${mergeDir}/${index}.png`, (err) => {err && console.log(err);} )
        })
    }
}

mergeBuildedAssets();