const fs = require("fs");
const path = require("path");
const basePath = process.cwd();

const {
  collectionSize,
  buildDir,
  imgDir,
  jsonDir,
} = require(`${basePath}/src/config.js`);

const generateUniqueNumbers = (arrSize) => {
  const uniqueArr = [];
  while(uniqueArr.length < arrSize){
    var r = Math.floor(Math.random() * arrSize);
    if(uniqueArr.indexOf(r) === -1) uniqueArr.push(r);
  }
  return uniqueArr;
};

const moveImage = (edition, newEdition) => {
  fs.copyFile(`${imgDir}/${edition}.png`, `${buildDir}/images/${newEdition}.png` , () =>{});
}

const replaceMetadata = (metadata, edition) => {
  Object.assign(metadata, {
    name: metadata.name.split("#")[0] + "#" +edition,
    image: `${edition}.png`,
    edition,
    properties: {
      files: [{
        uri: `${edition}.png`,
        type: "image/png",
      }],
    }
  })
};

const randomizeFilesOrder = async () => {
  const uniqueArr = generateUniqueNumbers(collectionSize);

  if (fs.existsSync(buildDir)) {
    fs.rmdirSync(buildDir, { recursive: true });
  }

  fs.mkdirSync(buildDir);
  fs.mkdirSync(`${buildDir}/json`);
  fs.mkdirSync(`${buildDir}/images`);

  fs.readdirSync(jsonDir).forEach(file => {
    if(path.extname(file) !== ".json") return

    const fileData = JSON.parse(fs.readFileSync(`${jsonDir}/${file}`), "utf-8");
    const fileEdition = fileData.edition;    

    const newEdition = uniqueArr.splice(0, 1)[0];

    replaceMetadata(fileData, newEdition);
    moveImage(fileEdition, newEdition);

    fs.writeFileSync(`${buildDir}/json/${newEdition}.json`, JSON.stringify(fileData));
  });
};

randomizeFilesOrder();

