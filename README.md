# Welcome to MixMyFiles

## How to use

1. Place assets forlder to the project root

2. Change config file ```src/config.ts```

3. Run generation script:

```sh
yarn gen
```

4. If you need all assets in one folder, run:
```sh
yarn merge
```

5. If you need all assets in one folder without copying:
```sh
yarn merge:clean
```